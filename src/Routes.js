import React, {Component} from "react";
import Home from './Components/Home'
import About from './Components/About'
import ClassList from './Components/ClassList'
import { Route, Link, Switch } from "react-router-dom";

class Routes extends Component {
  render() {
    return (
      <React.Fragment>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
          
        </ul>

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about/:patito" component={About}/>
          <Route path="/classlist/:class" component={ClassList}/>
          {/* <Route component={NoMatch} /> */}
        </Switch>
      </React.Fragment>
    );
  }
}

export default Routes;
