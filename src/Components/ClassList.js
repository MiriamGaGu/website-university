import React, { Component } from 'react';


class ClassList extends Component {
    constructor(){
        super();

        this.state = {
            students: []
        }
    }
    componentDidMount(){
        this.getClass();
    }
    
    getClass(){
        fetch('http://localhost:3000/db')
        .then(res => res.json())
        .then(data => {
            
            this.setState({
                students: data
            })
            
        })
    }
  getStudentsClasees(c) {
    var found = this.state.students.filter(s => s.id === parseInt(c));

    return found[0].class;
  }

  render() {
    var { classes } = this.props.match.params;

    return (
      <h2>Class: <strong>{ this.getStudentsClasees(classes) }</strong></h2>
    );
  }
}

export default ClassList;
