import React, { Component } from 'react';
import { Link } from "react-router-dom";


class Home extends Component {
    constructor(){
        super();

        this.state = {
            students: []
        }
    }
    componentDidMount(){
        this.getStudentsList();
    }
    
    getStudentsList(){
        fetch('http://localhost:3000/db')
        .then(res => res.json())
        .then(data => {

            this.setState({
                students: data
            })
            
        })
    }
       
    
        

  render() {
    // console.log(this.state.students)
    return (
        
        <ul>
        { this.state.students.map(s => {
          return (
            <li key={ s.id }>
              <Link to={ "/classlist/" + s.id }>{ s.class }</Link>
            </li>
          );
        }) }
      </ul>
      
    );
  }
}

export default Home;